-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 16-Fev-2017 às 02:30
-- Versão do servidor: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sao_francisco`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `bkp_produtos`
--

CREATE TABLE `bkp_produtos` (
  `id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(50) CHARACTER SET utf8 NOT NULL,
  `name` char(255) CHARACTER SET utf8 NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `price` decimal(25,2) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT 'no_image.png',
  `tax` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `cost` decimal(25,2) DEFAULT NULL,
  `tax_method` tinyint(1) DEFAULT '1',
  `quantity` decimal(15,2) DEFAULT '0.00',
  `barcode_symbology` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'code39',
  `type` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'standard',
  `details` text CHARACTER SET utf8,
  `alert_quantity` decimal(10,2) DEFAULT '0.00',
  `cozinha` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `bkp_produtos`
--

INSERT INTO `bkp_produtos` (`id`, `code`, `name`, `category_id`, `price`, `image`, `tax`, `cost`, `tax_method`, `quantity`, `barcode_symbology`, `type`, `details`, `alert_quantity`, `cozinha`) VALUES
(1, '0001', 'Hamburquer', 2, '2.00', '99ba81363ddbfe5a92c93023e1fd550a.jpg', '0', '4.00', 0, '53.00', 'code39', 'standard', 'Hamburguer com P?o de Hamburguer, queijo, carne, presunto e salada', '5.00', 1),
(2, '0002', 'Mixto Quente', 2, '1.00', '3ba18844e23b27e8224f8fa6b1752208.jpg', '0', '3.00', 0, '8.00', 'code39', 'standard', '', '5.00', 1),
(3, '0003', 'Cahorro Quente', 2, '2.00', '573bc5101fabefd864960416b1752899.jpg', '0', '3.00', 0, '4.00', 'code39', 'standard', '', '5.00', 1),
(4, '0004', 'Bolo de Chocolate', 2, '2.00', '8ad58758122f3a886e859def53da6a6a.jpg', '0', '3.00', 0, '7.00', 'code39', 'standard', '', '5.00', 0),
(5, '0005', 'Coxinha de Frango', 2, '2.00', 'd3115abf501ce492bdf449f72f185fb1.jpg', '0', '3.00', 0, '9.00', 'code39', 'standard', '', '5.00', 0),
(6, '0006', 'Empada', 2, '2.00', '76fed631b7861010869172aa83d78e0a.jpg', '0', '3.00', 0, '19.00', 'code39', 'standard', '', '5.00', 0),
(7, '0007', 'Monteiro Lopes', 2, '2.00', '3274477f5b7d3ef257c4562c56ef387e.jpg', '0', '3.00', 0, '10.00', 'code39', 'standard', '', '5.00', 0),
(8, '0008', 'Risole de Carne', 2, '2.00', '32a3ac97716a9dc68812aecbaf11840a.jpg', '0', '4.00', 0, '4.00', 'code39', 'standard', '', '5.00', 0),
(9, '0009', 'Coxinha de Caranguejo', 2, '4.00', '8bd5b89b645b1bc2d4d08816b5ad3d0b.jpg', '0', '6.00', 0, '6.00', 'code39', 'standard', '', '5.00', 0),
(10, '0010', 'Coxinha de Camar?o', 2, '4.00', '272825062f261b126f1996ed099b4b87.jpg', '0', '6.00', 0, '7.00', 'code39', 'standard', '', '5.00', 0),
(11, '0011', 'Sonho', 2, '2.00', '1f56837339171226e7e33eb0c5e8eae0.jpg', '0', '3.00', 0, '7.00', 'code39', 'standard', '', '5.00', 0),
(12, '0012', 'Lasanha', 2, '6.00', 'fd1c25461a5fbb0597c68bb78100c6ec.jpg', '0', '9.00', 0, '10.00', 'code39', 'standard', '', '5.00', 0),
(13, '0013', 'Torta de Chocolate', 2, '3.00', '11fcdf61a2d8c2d6b7c3e9c0a6996a54.jpg', '0', '6.00', 0, '10.00', 'code39', 'standard', '', '5.00', 0),
(14, '0014', 'Fanta Laranja Lata', 1, '2.00', 'f0ed23add960528f5da95d8fb2a8a106.jpg', '0', '4.00', 0, '10.00', 'code39', 'standard', '', '5.00', 0),
(15, '0015', 'Coca-Cola Lata', 1, '2.00', 'd1ae8344e2fdfc3fcd80a96bb1f00240.jpg', '0', '4.00', 0, '7.00', 'code39', 'standard', '', '5.00', 0),
(16, '0016', '?gua Mineral', 1, '2.00', '91b3bcff369f45e167c3544bad752912.jpg', '0', '3.00', 0, '9.00', 'code39', 'standard', '', '5.00', 0),
(17, '0017', 'Suco de Laranja', 1, '4.00', 'f4cab501731cb47389a6c1a9a54cf736.jpg', '0', '6.00', 0, '5.00', 'code39', 'standard', '', '5.00', 0),
(18, '01', 'Combo M', 2, '10.99', 'no_image.png', '5', '8.71', 0, '0.00', 'code39', 'combo', '', '0.00', 0),
(19, '02', 'Batata M', 2, '7.99', 'no_image.png', '0', '4.72', 0, '0.00', 'code39', 'standard', '', '0.00', 0),
(20, '03', 'Cobertura Cheddar', 2, '1.00', 'no_image.png', '0', '0.27', 0, '0.00', 'code39', 'standard', '', '0.00', 0),
(21, '012', 'Pizza Calabresa', 3, '24.00', 'no_image.png', '0', '12.00', 1, '10.00', 'code39', 'standard', '', '1.00', 1),
(22, '20', 'Pizza Mussarela', 3, '26.00', 'no_image.png', '0', '13.00', 1, '10.00', 'code39', 'standard', '', '0.00', 1),
(23, '027', '1/2 Pizza Mussarela', 3, '26.00', 'no_image.png', '0', '6.00', 0, '11.00', 'code39', 'standard', '', '0.00', 1),
(24, '01777', '1/2 Calabresa', 3, '21.00', 'no_image.png', '0', '6.00', 0, '9.00', 'code39', 'standard', '', '0.00', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `caixa01`
--

CREATE TABLE `caixa01` (
  `id` int(11) NOT NULL,
  `data_abertura` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `data_fechamento` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `valor_inicial` decimal(10,2) NOT NULL,
  `valor_final` decimal(10,2) NOT NULL,
  `status` varchar(45) CHARACTER SET utf8 NOT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `caixa01`
--

INSERT INTO `caixa01` (`id`, `data_abertura`, `data_fechamento`, `valor_inicial`, `valor_final`, `status`, `id_usuario`) VALUES
(1, '0000-00-00 00:00', '0000-00-00 00:00', '0.00', '0.00', 'Fechado', 1),
(19, '2017-02-15 14:55', '2017-02-15 14:55', '100.00', '100.00', 'Fechado', 1),
(20, '2017-02-15 19:08', '2017-02-15 23:26', '120.00', '280.00', 'Fechado', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `caixa02`
--

CREATE TABLE `caixa02` (
  `id` int(11) NOT NULL,
  `data_abertura` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `data_fechamento` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `valor_inicial` decimal(10,2) NOT NULL,
  `valor_final` decimal(10,2) NOT NULL,
  `status` varchar(45) CHARACTER SET utf8 NOT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `caixa02`
--

INSERT INTO `caixa02` (`id`, `data_abertura`, `data_fechamento`, `valor_inicial`, `valor_final`, `status`, `id_usuario`) VALUES
(1, '0000-00-00 00:00', '000-00-00 00:00', '0.00', '0.00', 'Fechado', 3),
(2, '2017-01-25 00:49', '000-00-00 00:00', '0.00', '0.00', 'Fechado', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cargo`
--

CREATE TABLE `cargo` (
  `id` int(11) NOT NULL,
  `cargo` varchar(45) NOT NULL,
  `permissao` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cargo`
--

INSERT INTO `cargo` (`id`, `cargo`, `permissao`) VALUES
(1, 'admin', 1),
(2, 'Gerente', 1),
(3, 'Operadora', 0),
(4, 'Garçom', 0),
(5, 'Motoboy', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `categoria` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `categoria`) VALUES
(1, 'Pizzas'),
(2, 'Esfihas'),
(3, 'Salgados'),
(4, 'Beirutes'),
(5, 'Porções'),
(6, 'Bebidas'),
(7, 'Pastéis'),
(8, 'Lanches'),
(9, 'Doces'),
(10, 'Sorvetes'),
(11, 'Balas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `name` varchar(55) CHARACTER SET utf8 NOT NULL,
  `cf1` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cf2` varchar(255) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 NOT NULL,
  `celular` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `taxa_de_entrega` decimal(10,2) NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `endereco` varchar(125) CHARACTER SET utf8 DEFAULT NULL,
  `bairro` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `cep` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
  `delivery` varchar(15) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `name`, `cf1`, `cf2`, `phone`, `celular`, `taxa_de_entrega`, `email`, `endereco`, `bairro`, `cep`, `delivery`) VALUES
(1, 'Cliente Padrão', '', '', '1122222222', '', '3.00', '', 'Rua teste, 123', 'Teste', '01112-23', ''),
(3, 'Junior', '', '', '11999233212', '', '2.00', '', 'Rua teste', 'TESTE', '', ''),
(6, 'Felipe', '', '', '121313213', '', '4.00', '', 'Rua Longe', 'Teste', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `ID` int(11) NOT NULL,
  `NOME` varchar(100) DEFAULT NULL,
  `FONE` varchar(15) NOT NULL,
  `CELULAR` varchar(15) NOT NULL,
  `EMAIL` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos`
--

CREATE TABLE `contatos` (
  `id` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `telefone` varchar(45) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contatos`
--

INSERT INTO `contatos` (`id`, `nome`, `email`, `telefone`) VALUES
(1, 'Diogo Cezar', 'xgordo@gmail.com', '(43) 3523-2956'),
(2, 'Mario Sergio', 'padariajoia@gmail.com', '(43) 9915-7944'),
(3, 'JoÃ£o da Silva', 'joao@gmail.com', '(41) 3453-9876'),
(4, 'Junior', 'teste', '1231231'),
(5, 'Felipe', 'teste12', '1241414'),
(6, 'outro gato', 'outro@teste', '7783894');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fechamentos`
--

CREATE TABLE `fechamentos` (
  `id` int(11) NOT NULL DEFAULT '0',
  `id_caixa` int(11) DEFAULT NULL,
  `data_abertura` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `data_fechamento` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `valor_inicial` decimal(10,2) NOT NULL,
  `valor_final` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `forma_pagamento`
--

CREATE TABLE `forma_pagamento` (
  `id` int(11) NOT NULL,
  `forma_pagamento` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `icone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `forma_pagamento`
--

INSERT INTO `forma_pagamento` (`id`, `forma_pagamento`, `icone`, `link`) VALUES
(1, 'Dinheiro', 'money icon', 'din_vendaDAO.php'),
(2, 'Cartão de Débito', 'payment icon', 'vendaDAO.php'),
(3, 'Cartão de Crédito', 'credit card alternative icon', 'vendaDAO.php'),
(4, 'Dinheiro + Débito', '', ''),
(5, 'Dinheiro + Crédito', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `movimentacao_caixa01`
--

CREATE TABLE `movimentacao_caixa01` (
  `id` int(11) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `tipo_movimentacao` varchar(2) NOT NULL,
  `saldo` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pde_fato_vendas`
--

CREATE TABLE `pde_fato_vendas` (
  `id` int(11) NOT NULL,
  `data_venda` varchar(60) NOT NULL,
  `origem_venda` varchar(60) NOT NULL,
  `num_nota_fiscal` int(11) NOT NULL,
  `id_forma_pagamento` int(11) NOT NULL,
  `id_abertura` int(11) DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pde_fato_vendas`
--

INSERT INTO `pde_fato_vendas` (`id`, `data_venda`, `origem_venda`, `num_nota_fiscal`, `id_forma_pagamento`, `id_abertura`, `status`) VALUES
(1, '2017-02-10 03:49', 'Caixa 01', 1, 3, 2, 'A'),
(43, '2017-02-15 21:43', 'Delivery', 2, 3, 20, 'A'),
(44, '2017-02-15 21:43', 'Delivery', 3, 2, 20, 'A'),
(45, '2017-02-15 21:44', 'Delivery', 4, 3, 20, 'A'),
(46, '2017-02-15 21:44', 'Delivery', 5, 2, 20, 'A'),
(47, '2017-02-15 22:43', 'Delivery', 6, 3, 20, 'A'),
(48, '2017-02-15 22:45', 'Delivery', 7, 3, 20, 'A');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pde_fato_vendas_produtos`
--

CREATE TABLE `pde_fato_vendas_produtos` (
  `id` int(11) NOT NULL,
  `num_nota_fiscal` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pde_fato_vendas_produtos`
--

INSERT INTO `pde_fato_vendas_produtos` (`id`, `num_nota_fiscal`, `id_produto`, `quantidade`, `obs`) VALUES
(1, 1, 1, 1, ''),
(56, 2, 530, 1, ''),
(57, 2, 5, 1, ''),
(58, 3, 532, 1, ''),
(59, 3, 2, 1, ''),
(60, 4, 530, 1, ''),
(61, 4, 9, 1, ''),
(62, 5, 530, 1, ''),
(63, 5, 9, 1, ''),
(64, 6, 530, 1, ''),
(65, 6, 47, 10, ''),
(66, 7, 530, 1, ''),
(67, 7, 12, 1, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pde_movimentacao`
--

CREATE TABLE `pde_movimentacao` (
  `id` int(11) NOT NULL,
  `tipo_movimentacao` varchar(2) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `origem` varchar(45) NOT NULL,
  `id_forma_pagamento` int(11) NOT NULL,
  `num_nota_fiscal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pde_movimentacao`
--

INSERT INTO `pde_movimentacao` (`id`, `tipo_movimentacao`, `valor`, `origem`, `id_forma_pagamento`, `num_nota_fiscal`) VALUES
(1, 'E', '28.00', 'Caixa 01', 3, 1),
(46, 'E', '27.00', 'Delivery', 3, 2),
(47, 'E', '28.00', 'Delivery', 2, 3),
(48, 'E', '30.00', 'Delivery', 3, 4),
(49, 'E', '30.00', 'Delivery', 2, 5),
(50, 'E', '15.00', 'Delivery', 3, 6),
(51, 'E', '30.00', 'Delivery', 3, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa1`
--

CREATE TABLE `pedido_aux_mesa1` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa2`
--

CREATE TABLE `pedido_aux_mesa2` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa3`
--

CREATE TABLE `pedido_aux_mesa3` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa4`
--

CREATE TABLE `pedido_aux_mesa4` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa5`
--

CREATE TABLE `pedido_aux_mesa5` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa6`
--

CREATE TABLE `pedido_aux_mesa6` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa8`
--

CREATE TABLE `pedido_aux_mesa8` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa11`
--

CREATE TABLE `pedido_aux_mesa11` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa12`
--

CREATE TABLE `pedido_aux_mesa12` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa14`
--

CREATE TABLE `pedido_aux_mesa14` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa15`
--

CREATE TABLE `pedido_aux_mesa15` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_aux_mesa19`
--

CREATE TABLE `pedido_aux_mesa19` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_balcao`
--

CREATE TABLE `pedido_balcao` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_balcao2`
--

CREATE TABLE `pedido_balcao2` (
  `id` int(11) NOT NULL DEFAULT '0',
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_delivery`
--

CREATE TABLE `pedido_delivery` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_motoboy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa1`
--

CREATE TABLE `pedido_mesa1` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa2`
--

CREATE TABLE `pedido_mesa2` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa3`
--

CREATE TABLE `pedido_mesa3` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa4`
--

CREATE TABLE `pedido_mesa4` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa5`
--

CREATE TABLE `pedido_mesa5` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa6`
--

CREATE TABLE `pedido_mesa6` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa8`
--

CREATE TABLE `pedido_mesa8` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa11`
--

CREATE TABLE `pedido_mesa11` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa12`
--

CREATE TABLE `pedido_mesa12` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa14`
--

CREATE TABLE `pedido_mesa14` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa15`
--

CREATE TABLE `pedido_mesa15` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_mesa19`
--

CREATE TABLE `pedido_mesa19` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) NOT NULL,
  `id_garcom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_suspensos`
--

CREATE TABLE `produtos_suspensos` (
  `id` int(11) NOT NULL,
  `id_suspensao` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `id_motoboy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos_suspensos`
--

INSERT INTO `produtos_suspensos` (`id`, `id_suspensao`, `id_produto`, `quantidade`, `id_motoboy`) VALUES
(38, 'ID582', 0, 0, 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sabores_pizza`
--

CREATE TABLE `sabores_pizza` (
  `id` int(11) NOT NULL,
  `sabor1` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `sabor2` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `sabor3` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sabores_pizza`
--

INSERT INTO `sabores_pizza` (`id`, `sabor1`, `sabor2`, `sabor3`) VALUES
(1, 'PIZZA MUSSARELA', 'PIZZA CALABRESA', ''),
(2, 'PIZZA DOIS QUEIJOS', 'PIZZA MILHO VERDE', ''),
(3, 'ATUM', 'CALABRESA', ''),
(4, 'MUSSARELA', 'ATUM', '4 QUEIJOS');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tabela_auxiliar_venda`
--

CREATE TABLE `tabela_auxiliar_venda` (
  `id` int(50) NOT NULL,
  `total` int(100) NOT NULL,
  `pago` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_mesas`
--

CREATE TABLE `tec_mesas` (
  `id` int(11) NOT NULL DEFAULT '0',
  `mesa` varchar(45) CHARACTER SET utf8 NOT NULL,
  `estado` varchar(45) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tec_mesas`
--

INSERT INTO `tec_mesas` (`id`, `mesa`, `estado`) VALUES
(1, 'Mesa 01', 'free'),
(2, 'Mesa 02', 'free'),
(3, 'Mesa 03', 'free'),
(4, 'Mesa 04', 'free'),
(5, 'Mesa 05', 'free'),
(6, 'Mesa 06', 'free'),
(7, 'Mesa 07', 'free'),
(8, 'Mesa 08', 'free'),
(9, 'Mesa 09', 'free'),
(10, 'Mesa 10', 'free'),
(11, 'Mesa 11', 'free'),
(12, 'Mesa 12', 'free'),
(13, 'Mesa 13', 'free'),
(14, 'Mesa 14', 'free'),
(15, 'Mesa 15', 'free'),
(16, 'Mesa 16', 'free'),
(17, 'Mesa 17', 'free'),
(18, 'Mesa 18', 'free'),
(19, 'Mesa 19', 'free'),
(20, 'Mesa 20', 'free'),
(21, 'Mesa 21', 'free'),
(22, 'Mesa 22', 'free'),
(23, 'Mesa 23', 'free'),
(24, 'Mesa 24', 'free'),
(25, 'Mesa 25', 'free'),
(26, 'Mesa 26', 'free'),
(27, 'Mesa 27', 'free');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_pedido_mesa`
--

CREATE TABLE `tec_pedido_mesa` (
  `id` int(11) NOT NULL DEFAULT '0',
  `id_produto` int(11) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `impresso` int(11) NOT NULL,
  `cozinha` int(11) DEFAULT NULL,
  `foi_pedido` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tec_products`
--

CREATE TABLE `tec_products` (
  `id` int(11) NOT NULL,
  `code` int(10) NOT NULL,
  `name` varchar(65) CHARACTER SET utf8 NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `price` decimal(25,2) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT 'no_image.png',
  `tax` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `cost` decimal(25,2) DEFAULT NULL,
  `tax_method` tinyint(1) DEFAULT '1',
  `quantity` decimal(15,2) DEFAULT '0.00',
  `barcode_symbology` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'code39',
  `type` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'standard',
  `details` text CHARACTER SET utf8,
  `alert_quantity` decimal(10,2) DEFAULT '0.00',
  `cozinha` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tec_products`
--

INSERT INTO `tec_products` (`id`, `code`, `name`, `category_id`, `price`, `image`, `tax`, `cost`, `tax_method`, `quantity`, `barcode_symbology`, `type`, `details`, `alert_quantity`, `cozinha`) VALUES
(1, 101, 'PIZZA AMERICANA', 1, '0.00', 'pamericana.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(2, 102, 'PIZZA ATUM I', 1, '0.00', 'patum.jpg', '0', '24.00', 0, '5.00', '', '', '', '2.00', 1),
(3, 103, 'PIZZA ATUM II', 1, '0.00', 'patum.jpg', '0', '25.00', 0, '5.00', '', '', '', '2.00', 1),
(4, 104, 'PIZZA BACON', 1, '0.00', 'baconqueijo.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(5, 105, 'PIZZA BAURU', 1, '0.00', 'pbauru.jpg', '0', '25.00', 0, '5.00', '', '', '', '2.00', 1),
(6, 106, 'PIZZA BAIANA', 1, '0.00', 'pbaiana.jpg', '0', '24.00', 0, '5.00', '', '', '', '2.00', 1),
(7, 107, 'PIZZA BARCELONA', 1, '0.00', 'pcatoles.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(8, 108, 'PIZZA BRASILEIRA', 1, '0.00', 'patum.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(9, 109, 'PIZZA BROCOLIS', 1, '0.00', 'pbrocolisrequeijao.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(10, 110, 'PIZZA CALABRESA I', 1, '0.00', 'pcalabresa.jpg', '0', '23.90', 0, '5.00', '', '', '', '2.00', 1),
(11, 111, 'PIZZA CALABRESA II', 1, '0.00', 'pcalabresa.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(12, 112, 'PIZZA MODA DA CASA', 1, '0.00', 'pmoda.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(13, 113, 'PIZZA CAIPIRA I', 1, '0.00', 'pcaipira.jpg', '0', '29.00', 0, '5.00', '', '', '', '2.00', 1),
(14, 114, 'PIZZA CAIPIRA II', 1, '0.00', 'pcaipira.jpg', '0', '30.00', 0, '5.00', '', '', '', '2.00', 1),
(15, 115, 'PIZZA CAMPONESA', 1, '0.00', 'pbrasileira.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(16, 116, 'PIZZA CARNE SECA', 1, '0.00', 'pcarneseca.jpg', '0', '36.00', 0, '5.00', '', '', '', '2.00', 1),
(17, 117, 'PIZZA DOIS QUEIJOS', 1, '0.00', 'pmussarela.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(18, 118, 'PIZZA ESCAROLA', 1, '0.00', 'pescarola.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(19, 119, 'PIZZA ESPANHOLA', 1, '0.00', 'pespanhola.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(20, 120, 'PIZZA FRANGO C/ CATUPIRY', 1, '0.00', 'pfrangocatupiry.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(21, 121, 'PIZZA FRANCESINHA', 1, '0.00', 'ppresunto.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(22, 122, 'PIZZA GIANINNA', 1, '0.00', 'pvegetariana.jpg', '0', '30.00', 0, '5.00', '', '', '', '2.00', 1),
(23, 123, 'PIZZA GRAMUTE', 1, '0.00', 'ppresunto.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(24, 124, 'PIZZA HARLEY', 1, '0.00', 'pgrega.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(25, 125, 'PIZZA JARDINEIRA', 1, '0.00', 'pjardineira.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(26, 126, 'PIZZA LOMBO I', 1, '0.00', 'plombo.jpg', '0', '25.00', 0, '5.00', '', '', '', '2.00', 1),
(27, 127, 'PIZZA LOMBO II', 1, '0.00', 'plombo.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(28, 128, 'PIZZA MARGUERITA', 1, '0.00', 'pmarguerita.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(29, 129, 'PIZZA MILHO', 1, '0.00', 'pmilho.jpg', '0', '25.00', 0, '5.00', '', '', '', '2.00', 1),
(30, 130, 'PIZZA MEXICO', 1, '0.00', 'plombo.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(31, 131, 'PIZZA MUSSARELA', 1, '0.00', 'pmussarela.jpg', '0', '23.90', 0, '5.00', '', '', '', '2.00', 1),
(32, 132, 'PIZZA NAPOLITANA', 1, '0.00', 'p2queijos.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(33, 133, 'PIZZA PALMITO', 1, '0.00', 'ppalmito.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(34, 134, 'PIZZA PEPERONI', 1, '0.00', 'ppeperoni.jpg', '0', '36.00', 0, '5.00', '', '', '', '2.00', 1),
(35, 135, 'PIZZA PERUANA', 1, '0.00', 'pgrega.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(36, 136, 'PIZZA PORTUGUESA', 1, '0.00', 'pportuguesa.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(37, 137, 'PIZZA PORTUGUESA ESPECIAL', 1, '0.00', 'pportuguesa.jpg', '0', '32.00', 0, '5.00', '', '', '', '2.00', 1),
(38, 138, 'PIZZA QUATRO QUEIJOS', 1, '0.00', 'p4queijos.jpg', '0', '30.00', 0, '5.00', '', '', '', '2.00', 1),
(39, 139, 'PIZZA SÃO FRANCISCO', 1, '0.00', 'p3queijos.jpg', '0', '32.00', 0, '5.00', '', '', '', '2.00', 1),
(40, 140, 'PIZZA SICILIANA', 1, '0.00', 'psiciliana.jpg', '0', '29.00', 0, '5.00', '', '', '', '2.00', 1),
(41, 141, 'PIZZA TOSCANA', 1, '0.00', 'ptoscana.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(42, 142, 'PIZZA TRES QUEIJOS', 1, '0.00', 'p3queijos.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(43, 143, 'PIZZA VENEZA', 1, '0.00', 'ptropical.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(44, 144, 'PIZZA BRIGADEIRO', 1, '0.00', 'pbrigadeiro.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(45, 145, 'PIZZA MMs', 1, '0.00', 'pmm.jpg', '0', '32.00', 0, '5.00', '', '', '', '2.00', 1),
(46, 146, 'PIZZA ROMEU E JULIETA', 1, '0.00', 'promeuejulieta.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(47, 201, 'ESFIHA CARNE', 2, '0.00', 'carne.jpg', '0', '1.30', 0, '5.00', '', '', '', '2.00', 1),
(48, 202, 'ESFIHA ATUM', 2, '0.00', 'atum.jpg', '0', '1.50', 0, '5.00', '', '', '', '2.00', 1),
(49, 203, 'ESFIHA CALABRESA', 2, '0.00', 'calabresa.jpg', '0', '1.50', 0, '5.00', '', '', '', '2.00', 1),
(50, 204, 'ESFIHA QUEIJO MUSSARELA', 2, '0.00', 'queijo.jpg', '0', '1.50', 0, '5.00', '', '', '', '2.00', 1),
(51, 205, 'ESFIHA CALABRESA C/ CATUPIRY', 2, '0.00', 'calabresaqueijo.jpg', '0', '2.00', 0, '5.00', '', '', '', '2.00', 1),
(52, 206, 'ESFIHA TOSCANA', 2, '0.00', 'ptoscana.jpg', '0', '2.00', 0, '5.00', '', '', '', '2.00', 1),
(53, 207, 'ESFIHA PALMITO C/ CATUPIRY', 2, '0.00', 'palmito.jpg', '0', '2.00', 0, '5.00', '', '', '', '2.00', 1),
(54, 208, 'ESFIHA PALMITO C/ MUSSARELA', 2, '0.00', 'palmito.jpg', '0', '2.00', 0, '5.00', '', '', '', '2.00', 1),
(55, 209, 'ESFIHA ATUM C/ CATUPIRY', 2, '0.00', 'atum.jpg', '0', '2.00', 0, '5.00', '', '', '', '2.00', 1),
(56, 210, 'ESFIHA FRANGO C/ CATUPIRY', 2, '0.00', 'frangorequeijao.jpg', '0', '2.00', 0, '5.00', '', '', '', '2.00', 1),
(57, 211, 'ESFIHA FRANGO C/ MUSSARELA', 2, '0.00', 'frangorequeijao.jpg', '0', '2.00', 0, '5.00', '', '', '', '2.00', 1),
(58, 212, 'ESFIHA BACON', 2, '0.00', 'baconqueijo.jpg', '0', '2.00', 0, '5.00', '', '', '', '2.00', 1),
(59, 213, 'ESFIHA ESCAROLA C/ MUSSARELA', 2, '0.00', 'escarola.jpg', '0', '2.00', 0, '5.00', '', '', '', '2.00', 1),
(60, 214, 'ESFIHA ESCAROLA C/ CATUPIRY', 2, '0.00', 'escarola.jpg', '0', '2.00', 0, '5.00', '', '', '', '2.00', 1),
(61, 215, 'ESFIHA CALABRESA C/ MUSSARELA', 2, '0.00', 'calabresaqueijo.jpg', '0', '2.00', 0, '5.00', '', '', '', '2.00', 1),
(62, 216, 'ESFIHA BAINA', 2, '0.00', 'bauru.jpg', '0', '2.00', 0, '5.00', '', '', '', '2.00', 1),
(63, 301, 'COXINHA DE FRANGO', 3, '0.00', 'coxinhafrango.jpg', '0', '2.50', 0, '5.00', '', '', '', '2.00', 1),
(64, 302, 'COXINHA FRANGO C/ CAUPIRY', 3, '0.00', 'coxinhafrango.jpg', '0', '3.00', 0, '5.00', '', '', '', '2.00', 1),
(65, 303, 'KIBE RECHADO', 3, '0.00', 'kibe.jpg', '0', '3.00', 0, '5.00', '', '', '', '2.00', 1),
(66, 304, 'BOLINHO DE CARNE', 3, '0.00', 'bolinhocarne.jpg', '0', '2.50', 0, '5.00', '', '', '', '2.00', 1),
(67, 305, 'RISOLE PRES/QUEIJO', 3, '0.00', 'risole.jpg', '0', '2.50', 0, '5.00', '', '', '', '2.00', 1),
(68, 401, 'BEIRUTE ESPECIAL', 4, '0.00', 'bfrango.jpg', '0', '20.00', 0, '5.00', '', '', '', '2.00', 1),
(69, 402, 'BEIRUTE ROSBIFE', 4, '0.00', 'bmoda.jpg', '0', '18.00', 0, '5.00', '', '', '', '2.00', 1),
(70, 403, 'BEIRUTE FRANGO', 4, '0.00', 'bcalabresacatupiry.jpg', '0', '18.00', 0, '5.00', '', '', '', '2.00', 1),
(71, 404, 'BEIRUTE CALABRESA', 4, '0.00', 'bcalabresa.jpg', '0', '18.00', 0, '5.00', '', '', '', '2.00', 1),
(72, 501, 'BATATA PEQUENA', 5, '0.00', 'porcaofritas.jpg', '0', '5.00', 0, '5.00', '', '', '', '2.00', 1),
(73, 502, 'BATATA MEDIA', 5, '0.00', 'porcaofritas.jpg', '0', '10.00', 0, '5.00', '', '', '', '2.00', 1),
(74, 503, 'BATATA GRANDE', 5, '0.00', 'porcaofritas.jpg', '0', '15.00', 0, '5.00', '', '', '', '2.00', 1),
(75, 601, 'COCA 2L', 6, '0.00', 'coca2.jpg', '0', '10.00', 0, '5.00', '', '', '', '2.00', 1),
(76, 602, 'COCA 1,5L', 6, '0.00', 'coca15.jpg', '0', '8.00', 0, '5.00', '', '', '', '2.00', 1),
(77, 603, 'COCA 600ML', 6, '0.00', 'coca600.jpg', '0', '5.00', 0, '5.00', '', '', '', '2.00', 1),
(78, 604, 'REFRIGERANTES 2L', 6, '0.00', 'refri2.jpg', '0', '8.00', 0, '5.00', '', '', '', '2.00', 1),
(79, 605, 'ITUBAINA 2L', 6, '0.00', 'itubaina2l.jpg', '0', '6.00', 0, '5.00', '', '', '', '2.00', 1),
(80, 606, 'DOLLY 2L', 6, '0.00', 'dolly2.jpg', '0', '5.00', 0, '5.00', '', '', '', '2.00', 1),
(81, 607, 'REFRIGERANTES LATA', 6, '0.00', 'minilata.jpg', '0', '4.00', 0, '5.00', '', '', '', '2.00', 1),
(82, 608, 'SKOL', 6, '0.00', 'skollata.jpg', '0', '4.00', 0, '5.00', '', '', '', '2.00', 1),
(83, 609, 'ITAIPAVA', 6, '0.00', 'itaipavalata.jpg', '0', '3.50', 0, '5.00', '', '', '', '2.00', 1),
(84, 610, 'DOLLY PROMOCAO', 6, '0.00', 'dolly2.jpg', '0', '0.00', 0, '5.00', '', '', '', '2.00', 1),
(85, 300, 'OPCIONAL', 2, '0.00', 'no_image.png', '0', '2.00', 0, '5.00', '', '', '', '2.00', 1),
(424, 10, 'AMERICANA', 98, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(425, 11, 'ATUM I', 98, '0.00', 'no_image.jpg', '0', '24.00', 0, '5.00', '', '', '', '2.00', 1),
(426, 12, 'ATUM II', 98, '0.00', 'no_image.jpg', '0', '25.00', 0, '5.00', '', '', '', '2.00', 1),
(427, 13, 'BACON', 98, '0.00', 'no_image.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(428, 14, 'BAURU', 98, '0.00', 'no_image.jpg', '0', '25.00', 0, '5.00', '', '', '', '2.00', 1),
(429, 15, 'BAIANA', 98, '0.00', 'no_image.jpg', '0', '24.00', 0, '5.00', '', '', '', '2.00', 1),
(430, 16, 'BARCELONA', 98, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(431, 17, 'BRASILEIRA', 98, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(432, 18, 'BROCOLIS', 98, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(433, 19, 'CALABRESA I', 98, '0.00', 'no_image.jpg', '0', '23.90', 0, '5.00', '', '', '', '2.00', 1),
(434, 20, 'CALABRESA II', 98, '0.00', 'no_image.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(435, 21, 'MODA DA CASA', 98, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(436, 22, 'CAIPIRA I', 98, '0.00', 'no_image.jpg', '0', '29.00', 0, '5.00', '', '', '', '2.00', 1),
(437, 23, 'CAIPIRA II', 98, '0.00', 'no_image.jpg', '0', '30.00', 0, '5.00', '', '', '', '2.00', 1),
(438, 24, 'CAMPONESA', 98, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(439, 25, 'CARNE SECA', 98, '0.00', 'no_image.jpg', '0', '36.00', 0, '5.00', '', '', '', '2.00', 1),
(440, 26, 'DOIS QUEIJOS', 98, '0.00', 'no_image.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(441, 27, 'ESCAROLA', 98, '0.00', 'no_image.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(442, 28, 'ESPANHOLA', 98, '0.00', 'no_image.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(443, 29, 'FRANGO C/ CATUPIRY', 98, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(444, 30, 'FRANCESINHA', 98, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(445, 31, 'GIANINNA', 98, '0.00', 'no_image.jpg', '0', '30.00', 0, '5.00', '', '', '', '2.00', 1),
(446, 32, 'GRAMUTE', 98, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(447, 33, 'HARLEY', 98, '0.00', 'no_image.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(448, 34, 'JARDINEIRA', 98, '0.00', 'no_image.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(449, 35, 'LOMBO I', 98, '0.00', 'no_image.jpg', '0', '25.00', 0, '5.00', '', '', '', '2.00', 1),
(450, 36, 'LOMBO II', 98, '0.00', 'no_image.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(451, 37, 'MARGUERITA', 98, '0.00', 'no_image.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(452, 38, 'MILHO', 98, '0.00', 'no_image.jpg', '0', '25.00', 0, '5.00', '', '', '', '2.00', 1),
(453, 39, 'MEXICO', 98, '0.00', 'no_image.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(454, 40, 'MUSSARELA', 98, '0.00', 'no_image.jpg', '0', '23.90', 0, '5.00', '', '', '', '2.00', 1),
(455, 41, 'NAPOLITANA', 98, '0.00', 'no_image.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(456, 42, 'PALMITO', 98, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(457, 43, 'PEPERONI', 98, '0.00', 'no_image.jpg', '0', '36.00', 0, '5.00', '', '', '', '2.00', 1),
(458, 44, 'PERUANA', 98, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(459, 45, 'PORTUGUESA', 98, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(460, 46, 'PORTUGUESA ESPECIAL', 98, '0.00', 'no_image.jpg', '0', '32.00', 0, '5.00', '', '', '', '2.00', 1),
(461, 47, 'QUATRO QUEIJOS', 98, '0.00', 'no_image.jpg', '0', '30.00', 0, '5.00', '', '', '', '2.00', 1),
(462, 48, 'SÃO FRANCISCO', 98, '0.00', 'no_image.jpg', '0', '32.00', 0, '5.00', '', '', '', '2.00', 1),
(463, 49, 'SICILIANA', 98, '0.00', 'no_image.jpg', '0', '29.00', 0, '5.00', '', '', '', '2.00', 1),
(464, 50, 'TOSCANA', 98, '0.00', 'no_image.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(465, 51, 'TRES QUEIJOS', 98, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(466, 52, 'VENEZA', 98, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(467, 53, 'BRIGADEIRO', 98, '0.00', 'no_image.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(468, 54, 'MMs', 98, '0.00', 'no_image.jpg', '0', '32.00', 0, '5.00', '', '', '', '2.00', 1),
(469, 55, 'ROMEU E JULIETA', 98, '0.00', 'no_image.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(470, 56, 'AMERICANA', 99, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(471, 57, 'ATUM I', 99, '0.00', 'no_image.jpg', '0', '24.00', 0, '5.00', '', '', '', '2.00', 1),
(472, 58, 'ATUM II', 99, '0.00', 'no_image.jpg', '0', '25.00', 0, '5.00', '', '', '', '2.00', 1),
(473, 59, 'BACON', 99, '0.00', 'no_image.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(474, 60, 'BAURU', 99, '0.00', 'no_image.jpg', '0', '25.00', 0, '5.00', '', '', '', '2.00', 1),
(475, 61, 'BAIANA', 99, '0.00', 'no_image.jpg', '0', '24.00', 0, '5.00', '', '', '', '2.00', 1),
(476, 62, 'BARCELONA', 99, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(477, 63, 'BRASILEIRA', 99, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(478, 64, 'BROCOLIS', 99, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(479, 65, 'CALABRESA I', 99, '0.00', 'no_image.jpg', '0', '23.90', 0, '5.00', '', '', '', '2.00', 1),
(480, 66, 'CALABRESA II', 99, '0.00', 'no_image.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(481, 67, 'MODA DA CASA', 99, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(482, 68, 'CAIPIRA I', 99, '0.00', 'no_image.jpg', '0', '29.00', 0, '5.00', '', '', '', '2.00', 1),
(483, 69, 'CAIPIRA II', 99, '0.00', 'no_image.jpg', '0', '30.00', 0, '5.00', '', '', '', '2.00', 1),
(484, 70, 'CAMPONESA', 99, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(485, 71, 'CARNE SECA', 99, '0.00', 'no_image.jpg', '0', '36.00', 0, '5.00', '', '', '', '2.00', 1),
(486, 72, 'DOIS QUEIJOS', 99, '0.00', 'no_image.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(487, 73, 'ESCAROLA', 99, '0.00', 'no_image.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(488, 74, 'ESPANHOLA', 99, '0.00', 'no_image.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(489, 75, 'FRANGO C/ CATUPIRY', 99, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(490, 76, 'FRANCESINHA', 99, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(491, 77, 'GIANINNA', 99, '0.00', 'no_image.jpg', '0', '30.00', 0, '5.00', '', '', '', '2.00', 1),
(492, 78, 'GRAMUTE', 99, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(493, 79, 'HARLEY', 99, '0.00', 'no_image.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(494, 80, 'JARDINEIRA', 99, '0.00', 'no_image.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(495, 81, 'LOMBO I', 99, '0.00', 'no_image.jpg', '0', '25.00', 0, '5.00', '', '', '', '2.00', 1),
(496, 82, 'LOMBO II', 99, '0.00', 'no_image.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(497, 83, 'MARGUERITA', 99, '0.00', 'no_image.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(498, 84, 'MILHO', 99, '0.00', 'no_image.jpg', '0', '25.00', 0, '5.00', '', '', '', '2.00', 1),
(499, 85, 'MEXICO', 99, '0.00', 'no_image.jpg', '0', '27.00', 0, '5.00', '', '', '', '2.00', 1),
(500, 86, 'MUSSARELA', 99, '0.00', 'no_image.jpg', '0', '23.90', 0, '5.00', '', '', '', '2.00', 1),
(501, 87, 'NAPOLITANA', 99, '0.00', 'no_image.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(502, 88, 'PALMITO', 99, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(503, 89, 'PEPERONI', 99, '0.00', 'no_image.jpg', '0', '36.00', 0, '5.00', '', '', '', '2.00', 1),
(504, 90, 'PERUANA', 99, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(505, 91, 'PORTUGUESA', 99, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(506, 92, 'PORTUGUESA ESPECIAL', 99, '0.00', 'no_image.jpg', '0', '32.00', 0, '5.00', '', '', '', '2.00', 1),
(507, 93, 'QUATRO QUEIJOS', 99, '0.00', 'no_image.jpg', '0', '30.00', 0, '5.00', '', '', '', '2.00', 1),
(508, 94, 'SÃO FRANCISCO', 99, '0.00', 'no_image.jpg', '0', '32.00', 0, '5.00', '', '', '', '2.00', 1),
(509, 95, 'SICILIANA', 99, '0.00', 'no_image.jpg', '0', '29.00', 0, '5.00', '', '', '', '2.00', 1),
(510, 96, 'TOSCANA', 99, '0.00', 'no_image.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(511, 97, 'TRES QUEIJOS', 99, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(512, 98, 'VENEZA', 99, '0.00', 'no_image.jpg', '0', '28.00', 0, '5.00', '', '', '', '2.00', 1),
(513, 99, 'BRIGADEIRO', 99, '0.00', 'no_image.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(514, 100, 'MMs', 99, '0.00', 'no_image.jpg', '0', '32.00', 0, '5.00', '', '', '', '2.00', 1),
(515, 101, 'ROMEU E JULIETA', 99, '0.00', 'no_image.jpg', '0', '26.00', 0, '5.00', '', '', '', '2.00', 1),
(516, 0, '1/2 CALABRESA I 1/2 ATUM I', 100, '0.00', 'no_image.png', '0', '24.00', 0, '10.00', '', '', '', '5.00', 1),
(517, 0, '1/2 MUSSARELA 1/2 ', 100, '0.00', 'no_image.png', '0', '23.90', 0, '10.00', '', '', '', '5.00', 1),
(518, 0, '1/2 MUSSARELA 1/2 CALABRESA I', 100, '0.00', 'no_image.png', '0', '23.90', 0, '10.00', '', '', '', '5.00', 1),
(521, 0, '1/3 CALABRESA I 1/3 MUSSARELA 1/3 ATUM I', 101, '0.00', 'no_image.png', '0', '24.00', 0, '10.00', '', '', '', '5.00', 1),
(522, 0, '1/3 CALABRESA I 1/3 MUSSARELA 1/3 AMERICANA', 101, '0.00', 'no_image.png', '0', '28.00', 0, '10.00', '', '', '', '5.00', 1),
(523, 0, '1/3 MUSSARELA 1/3 CALABRESA I 1/3 ATUM I', 101, '0.00', 'no_image.png', '0', '24.00', 0, '10.00', '', '', '', '5.00', 1),
(524, 0, '1/3 PORTUGUESA 1/3 PORTUGUESA ESPECIAL 1/3 LOMBO I', 101, '0.00', 'no_image.png', '0', '32.00', 0, '10.00', '', '', '', '5.00', 1),
(525, 0, '1/3 PORTUGUESA ESPECIAL 1/3 CALABRESA I 1/3 MUSSARELA', 101, '0.00', 'no_image.png', '0', '32.00', 0, '10.00', '', '', '', '5.00', 1),
(527, 0, '1/2 CALABRESA I 1/2 MUSSARELA', 100, '0.00', 'no_image.png', '0', '23.90', 0, '10.00', '', '', '', '5.00', 1),
(528, 0, '1/3 AMERICANA 1/3 PORTUGUESA ESPECIAL 1/3 DOIS QUEIJOS', 101, '0.00', 'no_image.png', '0', '32.00', 0, '10.00', '', '', '', '5.00', 1),
(529, 999, 'Taxa de entrega', 102, '0.00', 'no_image.png', '0', '1.00', 0, '1.00', 'code39', 'standard', NULL, '1.00', 1),
(530, 999, 'Taxa de entrega', 102, '0.00', 'no_image.png', '0', '2.00', 0, '1.00', 'code39', 'standard', NULL, '1.00', 1),
(531, 999, 'Taxa de entrega', 102, '0.00', 'no_image.png', '0', '3.00', 0, '1.00', 'code39', 'standard', NULL, '1.00', 1),
(532, 999, 'Taxa de entrega', 102, '0.00', 'no_image.png', '0', '4.00', 0, '1.00', 'code39', 'standard', NULL, '1.00', 1),
(533, 999, 'Taxa de entrega', 102, '0.00', 'no_image.png', '0', '5.00', 0, '1.00', 'code39', 'standard', NULL, '1.00', 1),
(534, 999, 'Taxa de entrega', 102, '0.00', 'no_image.png', '0', '6.00', 0, '1.00', 'code39', 'standard', NULL, '1.00', 1),
(535, 999, 'Taxa de entrega', 102, '0.00', 'no_image.png', '0', '7.00', 0, '1.00', 'code39', 'standard', NULL, '1.00', 1),
(536, 999, 'Taxa de entrega', 102, '0.00', 'no_image.png', '0', '8.00', 0, '1.00', 'code39', 'standard', NULL, '1.00', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(100) CHARACTER SET utf8 NOT NULL,
  `usuario` varchar(50) CHARACTER SET utf8 NOT NULL,
  `senha` varchar(50) CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(150) CHARACTER SET utf8 NOT NULL,
  `id_cargo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `usuario`, `senha`, `avatar`, `id_cargo`) VALUES
(1, 'Junior Nascimento', 'jjunior', 'ejwkh24', 'images/boy.png', 1),
(12, 'Operadora 1 ', 'operadora', '12345678', 'images/girl.png', 2),
(14, 'Motoboy Teste', 'motoboy', '12345678', 'images/boy.png', 5),
(15, 'Motoboy 2 ', '', '', 'images/boy.png', 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendas_history`
--

CREATE TABLE `vendas_history` (
  `id` int(11) NOT NULL,
  `code` int(10) NOT NULL,
  `Produto` varchar(65) CHARACTER SET utf8 NOT NULL,
  `quantidade` int(11) NOT NULL,
  `Preço` decimal(25,2) DEFAULT NULL,
  `Total` decimal(35,2) DEFAULT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `id_produto` int(11) NOT NULL DEFAULT '0',
  `num_nota_fiscal` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vendas_history`
--

INSERT INTO `vendas_history` (`id`, `code`, `Produto`, `quantidade`, `Preço`, `Total`, `obs`, `category_id`, `id_produto`, `num_nota_fiscal`) VALUES
(1, 1, 'ESFIHA CARNE', 1, '0.99', '0.99', '', 1, 1, NULL),
(2, 575, 'AMERICANA', 1, '31.00', '31.00', '31.00', 99, 311, NULL),
(3, 580, 'BRASILEIRA', 1, '30.00', '30.00', '', 99, 316, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendas_motoboys`
--

CREATE TABLE `vendas_motoboys` (
  `id` int(11) NOT NULL,
  `id_motoboy` int(11) NOT NULL,
  `entregas` int(11) NOT NULL,
  `total_taxas` decimal(10,2) DEFAULT NULL,
  `id_abertura` int(11) NOT NULL,
  `horario` varchar(45) DEFAULT NULL,
  `pago` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `vendas_motoboys`
--

INSERT INTO `vendas_motoboys` (`id`, `id_motoboy`, `entregas`, `total_taxas`, `id_abertura`, `horario`, `pago`) VALUES
(1, 14, 1, '2.00', 20, '2017-02-15 21:43', 1),
(2, 14, 1, '4.00', 20, '2017-02-15 21:43', 1),
(3, 15, 1, '2.00', 20, '2017-02-15 21:44', 1),
(4, 15, 1, '2.00', 20, '2017-02-15 21:44', 1),
(5, 15, 1, '2.00', 20, '2017-02-15 22:45', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendas_suspensas`
--

CREATE TABLE `vendas_suspensas` (
  `id` int(11) NOT NULL,
  `id_suspensao` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_suspensao` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `total` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `venda_balcao_hist`
--

CREATE TABLE `venda_balcao_hist` (
  `id` int(11) NOT NULL,
  `num_venda` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `obs` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `data_venda` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `forma_pagamento` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `total_dinheiro` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `venda_balcao_hist`
--

INSERT INTO `venda_balcao_hist` (`id`, `num_venda`, `id_produto`, `quantidade`, `obs`, `data_venda`, `forma_pagamento`, `total_dinheiro`) VALUES
(1, 1, 1, 1, '', '2017-01-16 15:22', 'Cartão de Débito', NULL),
(2, 1, 15, 1, '', '2017-01-16 15:22', 'Cartão de Débito', NULL),
(3, 1, 5, 1, '', '2017-01-16 19:28', 'Dinheiro', NULL),
(4, 1, 17, 1, '', '2017-01-16 19:28', 'Dinheiro', NULL),
(5, 2, 3, 1, '', '2017-01-16 19:59', 'Dinheiro', NULL),
(6, 2, 4, 1, '', '2017-01-16 19:59', 'Dinheiro', NULL),
(7, 3, 5, 2, '', '2017-01-16 22:45', '', NULL),
(8, 4, 12, 1, '', '2017-01-16 22:47', '', NULL),
(9, 5, 10, 1, '', '2017-01-16 22:48', 'Cartão de Crédito', NULL),
(10, 6, 1, 2, '', '2017-01-21 17:26', 'Dinheiro   Crédito', NULL),
(11, 7, 5, 4, '', '2017-01-21 17:31', 'Dinheiro / Débito', NULL),
(12, 8, 5, 3, '', '2017-01-22 18:36', 'Dinheiro', NULL),
(13, 9, 8, 7, '', '2017-01-22 18:47', 'Dinheiro', NULL),
(14, 10, 1, 10, '', '2017-01-23 18:41', 'Dinheiro', NULL),
(15, 11, 2, 1, '', '2017-01-23 19:21', 'Dinheiro', NULL),
(16, 12, 5, 1, '', '2017-01-23 19:22', 'Dinheiro', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `caixa01`
--
ALTER TABLE `caixa01`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `caixa02`
--
ALTER TABLE `caixa02`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `forma_pagamento`
--
ALTER TABLE `forma_pagamento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movimentacao_caixa01`
--
ALTER TABLE `movimentacao_caixa01`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pde_fato_vendas`
--
ALTER TABLE `pde_fato_vendas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pde_fato_vendas_produtos`
--
ALTER TABLE `pde_fato_vendas_produtos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pde_movimentacao`
--
ALTER TABLE `pde_movimentacao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa1`
--
ALTER TABLE `pedido_aux_mesa1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa2`
--
ALTER TABLE `pedido_aux_mesa2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa3`
--
ALTER TABLE `pedido_aux_mesa3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa4`
--
ALTER TABLE `pedido_aux_mesa4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa5`
--
ALTER TABLE `pedido_aux_mesa5`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa6`
--
ALTER TABLE `pedido_aux_mesa6`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa8`
--
ALTER TABLE `pedido_aux_mesa8`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa11`
--
ALTER TABLE `pedido_aux_mesa11`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa12`
--
ALTER TABLE `pedido_aux_mesa12`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa14`
--
ALTER TABLE `pedido_aux_mesa14`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa15`
--
ALTER TABLE `pedido_aux_mesa15`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_aux_mesa19`
--
ALTER TABLE `pedido_aux_mesa19`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_balcao`
--
ALTER TABLE `pedido_balcao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_delivery`
--
ALTER TABLE `pedido_delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa1`
--
ALTER TABLE `pedido_mesa1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa2`
--
ALTER TABLE `pedido_mesa2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa3`
--
ALTER TABLE `pedido_mesa3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa4`
--
ALTER TABLE `pedido_mesa4`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa5`
--
ALTER TABLE `pedido_mesa5`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa6`
--
ALTER TABLE `pedido_mesa6`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa8`
--
ALTER TABLE `pedido_mesa8`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa11`
--
ALTER TABLE `pedido_mesa11`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa12`
--
ALTER TABLE `pedido_mesa12`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa14`
--
ALTER TABLE `pedido_mesa14`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa15`
--
ALTER TABLE `pedido_mesa15`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido_mesa19`
--
ALTER TABLE `pedido_mesa19`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produtos_suspensos`
--
ALTER TABLE `produtos_suspensos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sabores_pizza`
--
ALTER TABLE `sabores_pizza`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabela_auxiliar_venda`
--
ALTER TABLE `tabela_auxiliar_venda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_products`
--
ALTER TABLE `tec_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendas_history`
--
ALTER TABLE `vendas_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendas_motoboys`
--
ALTER TABLE `vendas_motoboys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendas_suspensas`
--
ALTER TABLE `vendas_suspensas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venda_balcao_hist`
--
ALTER TABLE `venda_balcao_hist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `caixa01`
--
ALTER TABLE `caixa01`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `caixa02`
--
ALTER TABLE `caixa02`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forma_pagamento`
--
ALTER TABLE `forma_pagamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `movimentacao_caixa01`
--
ALTER TABLE `movimentacao_caixa01`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pde_fato_vendas`
--
ALTER TABLE `pde_fato_vendas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `pde_fato_vendas_produtos`
--
ALTER TABLE `pde_fato_vendas_produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `pde_movimentacao`
--
ALTER TABLE `pde_movimentacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa1`
--
ALTER TABLE `pedido_aux_mesa1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa2`
--
ALTER TABLE `pedido_aux_mesa2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa3`
--
ALTER TABLE `pedido_aux_mesa3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa4`
--
ALTER TABLE `pedido_aux_mesa4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa5`
--
ALTER TABLE `pedido_aux_mesa5`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa6`
--
ALTER TABLE `pedido_aux_mesa6`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa8`
--
ALTER TABLE `pedido_aux_mesa8`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa11`
--
ALTER TABLE `pedido_aux_mesa11`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa12`
--
ALTER TABLE `pedido_aux_mesa12`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa14`
--
ALTER TABLE `pedido_aux_mesa14`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa15`
--
ALTER TABLE `pedido_aux_mesa15`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_aux_mesa19`
--
ALTER TABLE `pedido_aux_mesa19`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_balcao`
--
ALTER TABLE `pedido_balcao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_delivery`
--
ALTER TABLE `pedido_delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa1`
--
ALTER TABLE `pedido_mesa1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa2`
--
ALTER TABLE `pedido_mesa2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa3`
--
ALTER TABLE `pedido_mesa3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa4`
--
ALTER TABLE `pedido_mesa4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa5`
--
ALTER TABLE `pedido_mesa5`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa6`
--
ALTER TABLE `pedido_mesa6`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa8`
--
ALTER TABLE `pedido_mesa8`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa11`
--
ALTER TABLE `pedido_mesa11`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa12`
--
ALTER TABLE `pedido_mesa12`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa14`
--
ALTER TABLE `pedido_mesa14`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa15`
--
ALTER TABLE `pedido_mesa15`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pedido_mesa19`
--
ALTER TABLE `pedido_mesa19`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `produtos_suspensos`
--
ALTER TABLE `produtos_suspensos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `sabores_pizza`
--
ALTER TABLE `sabores_pizza`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tabela_auxiliar_venda`
--
ALTER TABLE `tabela_auxiliar_venda`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tec_products`
--
ALTER TABLE `tec_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=537;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `vendas_history`
--
ALTER TABLE `vendas_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vendas_motoboys`
--
ALTER TABLE `vendas_motoboys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `vendas_suspensas`
--
ALTER TABLE `vendas_suspensas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `venda_balcao_hist`
--
ALTER TABLE `venda_balcao_hist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
